#Follow given steps for install all required softwares

Step 1 : Install Rust
    Command -> curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

    Note :- When installed, it will add the following PATH — or similar — to your shell configurations which is good to know if you want to move that to your dotfiles or something.

    export PATH="$HOME/.cargo/bin:$PATH"

    You can check that rust is properly installed by running the following commands.

    rustup --version
    rustc --version
    cargo --version


Step 2 : Install Solana
    Command ->  sh -c "$(curl -sSfL https://release.solana.com/v1.9.4/install)"   

    Note :- Installing Solana will also add a new PATH to your shell configurations. Alternatively, depending on your system, it might ask you to manually update your PATH by providing you with a line to copy/paste.

    export PATH="$HOME/.local/share/solana/install/active_release/bin:$PATH"

    You can check that Solana is properly installed by running the following commands.

    solana --version

Step 3 : Use Solana locally
    Command ->  solana config set --url localhost  

Step 4 : Generate your local key pair

    First, you might want to check if you've already got a local key pair by running the following command.

    solana address

    If you get an error, that means you don't have one yet and so let's create a new one.

    Command -> solana-keygen new 

Step 5 : Install Anchor
    Command -> cargo install --git https://github.com/project-serum/anchor anchor-cli --locked   

    Note that you can also install Anchor using npm globally. 

    You may run the following command to check Anchor CLI is installed properly.

    anchor --version

Step 6 : Install yarn 
    Command ->   npm install -g yarn 

Step 7 : Install Vue CLI
    Coomand -> npm install -g @vue/cli@5.0.0-rc.1

    You can check the VueJS CLI tools are installed properly by running:

    vue --version

Step 8 : Enable Phantom and Solflare wallet extention in your browser.

#Follow given steps for run and deploy project
1. cd Project-Root-Directory
2. npm install
3. anchor build
4. anchor deploy
5. cd app
6. npm install
7. To run on localhost :-
    (i) cd ../
    (ii) solana config set --url localhost
    (iii) solana-test-validator  Note:- Never exit this terminal because it is our local ledger for storing data.
    (iv) anchor deploy
    (v) cd app
    (vi) npm run serve
   To run on devnet :- 
    (i) cd ../
    (ii) solana config set --url devnet
    (iii) anchor deploy
    (iv) cd app
    (v) npm run serve:devnet